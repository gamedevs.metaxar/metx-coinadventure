// const admin = require("firebase-admin");
// const jwt = require('jsonwebtoken');
// require('dotenv').config();
// const moment = require('moment-timezone');
// const timezone = 'Asia/Singapore';
// const axios = require('axios');

// const dailyScore = async (req, res) => {
//   try {
//     const { gameID, user } = req.params;
//     let newScore = parseInt(req.query.score);
//     const maxDailyScore = req.query.limit ? parseInt(req.query.limit) : null;

//     if (!gameID || !user || isNaN(newScore)) {
//       return res.status(400).json({
//         success: false,
//         message: 'Game ID, user, and score are required and must be numbers.'
//       });
//     }

//     const now = moment().tz(timezone);
//     const time = now.format('YYYY-MM-DD HH:mm:ss'); 
//     const ref = admin.database().ref(`${gameID}/${user}/b_Score`);

//     // Retrieve the current daily score and total score from the database
//     const scoreSnapshot = await ref.once('value');
//     let dailyScore = scoreSnapshot.child('a_DailyScore').val() || 0;
//     let totalScore = scoreSnapshot.child('d_TotalScore').val() || 0;

//     if (maxDailyScore !== null) {
//       // Check if the current daily score is already at the limit
//       if (dailyScore >= maxDailyScore) {
//         return res.status(400).json({
//           success: false,
//           message: "You have reached your score limit for today",
//           data: {
//             score: dailyScore,
//             time
//           }
//         });
//       }

//       // If adding the new score would exceed the maximum limit,
//       // adjust it so the daily score equals the limit
//       if (dailyScore + newScore > maxDailyScore) {
//         newScore = maxDailyScore - dailyScore; // Adjust newScore to limit the dailyScore
//       }
//     }

//     // Add new score to the daily score and total score
//     dailyScore += newScore;
//     totalScore += newScore;

//     await ref.update({ a_DailyScore: dailyScore, b_DS_Updated: time, d_TotalScore: totalScore, e_TS_Updated: time });

//     const viewModel = {
//       success: true,
//       message: "Daily and total scores updated",
//       data: {
//         dailyScore,
//         totalScore,
//         time
//       },
//     };

//     return res.status(200).json(viewModel);
//   } catch (error) {
//     console.error(error);
//     return res.status(500).json({
//       success: false,
//       message: "Internal server error"
//     });
//   }
// };


// const tokensRequest = async (req, res) => {
//   try {
//     const { gameID, user } = req.params;
//     const { hash } = req.query;
//     const now = moment().tz(timezone);
//     const time = now.format('YYYY-MM-DD HH:mm:ss'); 
//     const ref = admin.database().ref(`${gameID}/${user}`);
//     const tokensReqRef = ref.child('c_TokensReq');
//     const scoreRef = ref.child('b_Score');

//     // Retrieve the current total score from the database
//     const scoreSnapshot = await scoreRef.once('value');
//     let totalScore = scoreSnapshot.child('d_TotalScore').val();

//     // Ensure totalScore is a number, if not default it to 0
//     if (typeof totalScore !== 'number') {
//       totalScore = 0;
//     }

//     const amount = totalScore; // set amount equal to the total score

//     // Ensure there's a request for tokens and a hash provided
//     if (!amount || !hash) {
//       return res.status(200).json({
//         success: false,
//         message: "There is no token request made or no hash provided.",
//         data: {},
//       });
//     }

//     const tokensReqSnapshot = await tokensReqRef.once('value');
//     const existingHash = tokensReqSnapshot.val() ? tokensReqSnapshot.val().b_TxnHash : null;

//     // If there's already a hash, then we can't proceed with another request until it's processed
//     if (existingHash) {
//       return res.status(200).json({
//         success: false,
//         message: "A token request is already in process. Please wait until it's finished.",
//         data: {},
//       });
//     }

//     await Promise.all([
//       // Update the tokens request data
//       tokensReqRef.update({ a_TokensReq: amount, b_TxnHash: hash, c_TR_Updated: time }),
//       // Reset the total score to 0
//       scoreRef.update({ d_TotalScore: 0, e_TS_Updated: time })
//     ]);

//     const viewModel = {
//       success: true,
//       message: "Tokens Requested updated and total score reset",
//       data: {
//         amount,
//         hash,
//         time,
//       },
//     };
//     return res.status(200).json(viewModel);

//   } catch (error) {
//     console.error(error);
//     return res.status(500).send(error);
//   }
// };

// module.exports = {
//   dailyScore,
//   tokensRequest,
// };
