# This is backend API between MetX games and Firebase

This API created using Express.js and node.js

1. Open terminal, go to directory:

```
cd NodeJS-GameBackend
```

2. install required packages
```
npm install
```

3. run
```
node index.js
```

4. Your api url will be

```
http://localhost:5000/
```

3. Go to Postman, import `Backend Node.js Firebase DB.postman_collection` to test the api.
4. Go to Backend Node.js Firebase DB -> Variables. Change or replace if needed.
5. Proceed tu `PUT Login` request. Click `Send`.
6. Copy the `token` from json return status.
7. Go to Backend Node.js Firebase DB -> Authorization. Click drop-down **Type** to `Bearer Token`.
8. Paste `token` you copied from return json in `PUT Login` to **Token**.

**POSTMAN LINK**: https://www.postman.com/winter-trinity-659015/workspace/backend-metx-games/collection/25862457-9a18cc6a-1039-4fbd-a788-5f264dc6d8c1?action=share&creator=25862457

**REALTIME DB VIEW**: https://console.firebase.google.com/u/0/project/metx-demo/database/metx-demo-default-rtdb/data


## API that can be used

- ### PUT Login
    - generate jwt
    - reset daily score if new day
    - save data: time, username, token id
    - query para: username, token id
- ### GET UserDetails
    - fetch data: daily score, total score, txn hash, total claim
    - query para: null
    - required: jwt token
- ### PUT Daily Score
    - daily score prompt from game
    - daily score must not exceed limit
    - limit is set from game
    - total score is add up from daily score
    - save data: daily, total, timeds, timets
    - query para: dailyscore, limit
    - required: jwt token
- ### PUT Tokens Request
    - data from total score is transfer to tokens req
    - totalscore reset 0
    - hash gets from autosigner_distributetoken
    - save data: hash, tokensreq, time
    - query para: hash
    - required: jwt token
- ### GET Check Claim Status
    - get txn status
    - txn hash will get from the existing database, and the transaction api from autosigner will trigger
    - below and after 5 mins, if txn status success: reset daily score, reset tokens requested
    - below 5 mins,  txn status unsuccess: nothing happen
    - after 5 mins, txn status unsuccess: retrieve data from tokensreq to total score, tokensreq reset, daily no change 
    - post history from tokens requested
    - save data: time, total tokens claimed, resetted tokens requested, and update total score
    - query para: autosigner_url
    - required: jwt token