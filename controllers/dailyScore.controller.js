const admin = require("firebase-admin");
const jwt = require('jsonwebtoken');
require('dotenv').config();
const moment = require('moment-timezone');
const timezone = 'Asia/Singapore';
const axios = require('axios');

const dailyScore = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    let newScore = parseInt(req.query.score);
    const maxDailyScore = req.query.limit ? parseInt(req.query.limit) : null;

    if (!gameID || !user || isNaN(newScore)) {
      return res.status(400).json({
        success: false,
        message: 'Game ID, user, and score are required and must be numbers.'
      });
    }

    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss'); 
    const ref = admin.database().ref(`${gameID}/${user}/b_Score`);

    // Retrieve the current daily score and total score from the database
    const scoreSnapshot = await ref.once('value');
    let dailyScore = scoreSnapshot.child('a_DailyScore').val() || 0;
    let totalScore = scoreSnapshot.child('d_TotalScore').val() || 0;

    if (maxDailyScore !== null) {
      // Check if the current daily score is already at the limit
      if (dailyScore >= maxDailyScore) {
        return res.status(400).json({
          success: false,
          message: "You have reached your score limit for today",
          data: {
            score: dailyScore,
            time
          }
        });
      }

      // If adding the new score would exceed the maximum limit,
      // adjust it so the daily score equals the limit
      if (dailyScore + newScore > maxDailyScore) {
        newScore = maxDailyScore - dailyScore; // Adjust newScore to limit the dailyScore
      }
    }

    // Add new score to the daily score and total score
    dailyScore += newScore;
    totalScore += newScore;

    await ref.update({ a_DailyScore: dailyScore, b_DS_Updated: time, d_TotalScore: totalScore, e_TS_Updated: time });

    const viewModel = {
      success: true,
      message: "Daily and total scores updated",
      data: {
        dailyScore,
        totalScore,
        time
      },
    };

    return res.status(200).json(viewModel);
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      success: false,
      message: "Internal server error"
    });
  }
};

module.exports = {
  dailyScore,
};
