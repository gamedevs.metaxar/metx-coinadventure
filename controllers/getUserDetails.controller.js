const admin = require("firebase-admin");
require('dotenv').config();

const getUserDetails = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const ref = admin.database().ref(`${gameID}/${user}`);
    const snapshot = await ref.once("value");
    const userData = snapshot.val();
    return res.status(200).json(userData);
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};


module.exports = {
  getUserDetails,
};
