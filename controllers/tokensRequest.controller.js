const admin = require("firebase-admin");
const jwt = require('jsonwebtoken');
require('dotenv').config();
const moment = require('moment-timezone');
const timezone = 'Asia/Singapore';
const axios = require('axios');

const tokensRequest = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const { hash } = req.query;
    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss'); 
    const ref = admin.database().ref(`${gameID}/${user}`);
    const tokensReqRef = ref.child('c_TokensReq');
    const scoreRef = ref.child('b_Score');

    // Retrieve the current total score from the database
    const scoreSnapshot = await scoreRef.once('value');
    let totalScore = scoreSnapshot.child('d_TotalScore').val();

    // Ensure totalScore is a number, if not default it to 0
    if (typeof totalScore !== 'number') {
      totalScore = 0;
    }

    const amount = totalScore; // set amount equal to the total score

    // Ensure there's a request for tokens and a hash provided
    if (!amount || !hash) {
      return res.status(200).json({
        success: false,
        message: "There is no token request made or no hash provided.",
        data: {},
      });
    }

    const tokensReqSnapshot = await tokensReqRef.once('value');
    const existingHash = tokensReqSnapshot.val() ? tokensReqSnapshot.val().b_TxnHash : null;

    // If there's already a hash, then we can't proceed with another request until it's processed
    if (existingHash) {
      return res.status(200).json({
        success: false,
        message: "A token request is already in process. Please wait until it's finished.",
        data: {},
      });
    }

    await Promise.all([
      // Update the tokens request data
      tokensReqRef.update({ a_TokensReq: amount, b_TxnHash: hash, c_TR_Updated: time }),
      // Reset the total score to 0
      scoreRef.update({ d_TotalScore: 0, e_TS_Updated: time })
    ]);

    const viewModel = {
      success: true,
      message: "Tokens Requested updated and total score reset",
      data: {
        amount,
        hash,
        time,
      },
    };
    return res.status(200).json(viewModel);

  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

module.exports = {
  tokensRequest,
};
