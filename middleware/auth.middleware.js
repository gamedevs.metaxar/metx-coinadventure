const admin = require("firebase-admin");
const jwt = require('jsonwebtoken');
require('dotenv').config();
const moment = require('moment-timezone');
const timezone = 'Asia/Singapore';

// Define the secret key for JWT token generation
const secretKey = process.env.SECRET_KEY;

// Add JWT middleware to verify token before accessing protected routes
const verifyToken = (req, res, next) => {
  const authHeader = req.headers.authorization;
  if (typeof authHeader !== "undefined") {
    const token = authHeader.split(" ")[1];
    jwt.verify(token, secretKey, (err, decoded) => {
      if (err) {
        return res.sendStatus(403).json({
          success: false,
          message: "Invalid or expired token."
        });
      }
      req.decoded = decoded;
      next();
    });
  } else {
    res.status(401).json({
      success: false,
      message: "Authorization header is missing."
    });
  }
};

const login = async (req, res) => {
  try {
      const { gameID } = req.params;
      const { user, tokenID } = req.body;
      const now = moment().tz(timezone);
      const time = now.format('YYYY-MM-DD HH:mm:ss');   
      
      const ref = admin.database().ref(`${gameID}/${user}`);
      const generalRef = ref.child('a_General');
      const scoreRef = ref.child('b_Score');
      
      await generalRef.update({ a_TokenID: tokenID, b_LoginTime: time });

      if (!tokenID) {
        return res.status(400).json({
          success: false,
          message: 'token ID are required.'
        });
      }
      
      // Retrieve the current daily score and the last update time from the database
      const scoreSnapshot = await scoreRef.once('value');
      const lastUpdate = scoreSnapshot.child('b_DS_Updated').val();

      // Reset the daily score if it's a different day now
      if (lastUpdate && moment(lastUpdate).tz(timezone).isBefore(now, 'day')) {
        await scoreRef.update({ a_DailyScore: 0, b_DS_Updated: time });
      }
      
      const token = jwt.sign({ user }, secretKey, { expiresIn: '24h' }); // Generate token with user
      
      const viewModel = {
        success: true,
        message: "User login info updated successfully",
        data: {
          gameID,
          user,
          tokenID,
          time
        },
        token: token // Add token to response
      };
      return res.status(200).json({viewModel});

    } catch (error) {
      console.error(error);
      return res.status(500).json({
        success: false,
        message: "Internal server error"
      });
    }
};


module.exports = {
  verifyToken,
  login,
};
